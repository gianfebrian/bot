package source

type Sourcer interface {
	Read([]byte) ([]byte, error)
}
