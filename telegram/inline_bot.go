package telegram

import (
	"encoding/json"

	"gitlab.com/gianfebrian/bot/conversation"
	"gitlab.com/gianfebrian/bot/source"

	tb "gopkg.in/tucnak/telebot.v2"
)

// InlineBot represents telegram bot
type InlineBot struct {
	Config       []byte
	Bot          *tb.Bot
	Source       source.Sourcer
	Conversation conversation.ListenTalker
	data         map[string]interface{}
}

func (b *InlineBot) parseSource() error {
	var s map[string]interface{}

	data, err := b.Source.Read(b.Config)
	if err != nil {
		return err
	}

	return json.Unmarshal(data, &b.data)
}

func (b *InlineBot) setupInlineKeys(data map[string]interface{}) error {
	value, ok := data["value"]
	if !ok {
		return errors.New("")
	}

	label, ok := data["label"]
	if !ok {
		return errors.New("")
	}

	b.hookToReply()
	return nil
}

func (b *InlineBot) hookToReply(b tb.InlineButton, next [][]tb.InlineButton) {
	b.Bot.Handle(b, func(m *tb.Message) {
		ctx, _ := c.Listen(m.Text)
		replyText, _ := c.Talk(ctx)

		var replyMarkup *tb.ReplyMarkup
		if len(next) > 0 {
			replyMarkup = new(tb.ReplyMarkup)
			replyMarkup.InlineKeyboard = next
		}

		b.Bot.Send(m.Sender, replyText, replyMarkup)
	})
}

// Start starts and listens to incoming event
func (b *InlineBot) Start() error {
	return nil
}
