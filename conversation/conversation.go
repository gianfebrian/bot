package conversation

// Context represents conversation context. Do not confuse with go context.
type Context struct{}

// Listener wraps listen methods
type Listener interface {
	Listen(m []byte) (*Context, error)
}

// Talker wraps talk methods
type Talker interface {
	Talk(c *Context) ([]byte, error)
}

// ListenTalkDoner wraps listen and talk methods
type ListenTalkDoner interface {
	Listener
	Talker
}
