FROM golang:1.9

RUN mkdir -p /app && mkdir -p /etc/bot/configs

ADD ./bot /app

ADD ./configs /etc/bot/configs

ENTRYPOINT [ "/app/bot" ]