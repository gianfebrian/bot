package main

import (
	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigName("app")
	viper.AddConfigPath("/etc/bot/configs")
	viper.AddConfigPath("./configs")

	if err := viper.ReadInConfig(); err != nil {
		panic(err.Error())
	}
}

func main() {
	start()
}
