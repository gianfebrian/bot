package main

import (
	"bytes"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/antchfx/htmlquery"
	"github.com/astaxie/beego/httplib"
	"github.com/spf13/viper"

	tb "gopkg.in/tucnak/telebot.v2"
)

type qa struct {
	Question string
	Answer   string
}

func handler(bot *tb.Bot, answer string) func(c *tb.Callback) {
	return func(c *tb.Callback) {
		bot.Edit(c.Message, answer)
		bot.Respond(c)
	}
}

func start() {
	poller := new(tb.LongPoller)
	poller.Timeout = 10 * time.Second

	settings := tb.Settings{
		Token:  viper.GetString("telegram.bot.token"),
		Poller: poller,
	}

	bot, err := tb.NewBot(settings)
	if err != nil {
		log.Fatal(err)

		return
	}

	data, err := scrape()
	if err != nil {
		log.Fatal(err)
	}

	var inlineKeys [][]tb.InlineButton

	for i, d := range data {
		ib := tb.InlineButton{
			Unique: strconv.Itoa(i),
			Text:   d.Question,
			Data:   strconv.Itoa(i),
		}

		inlineKeys = append(inlineKeys, []tb.InlineButton{ib})
		bot.Handle(&ib, handler(bot, d.Answer))
	}

	bot.Handle(tb.OnText, func(msg *tb.Message) {
		replyMarkup := new(tb.ReplyMarkup)
		replyMarkup.InlineKeyboard = inlineKeys

		bot.Send(msg.Sender, "Hi! How can I help you?", replyMarkup)
	})

	bot.Start()
}

func scrape() ([]qa, error) {
	b, err := httplib.Get(viper.GetString("datasource.scraper.url")).Bytes()
	if err != nil {
		return nil, err
	}

	node, err := htmlquery.Parse(bytes.NewReader(b))
	if err != nil {
		return nil, err
	}

	out := make([]qa, 0)
	for _, questionNode := range htmlquery.Find(node, `//h3`) {
		question := strings.TrimSpace(htmlquery.InnerText(questionNode))

		answerNode := htmlquery.FindOne(questionNode, `//following-sibling::div`)

		answer := ""
		if answerNode != nil {
			answer = strings.TrimSpace(htmlquery.InnerText(answerNode))
		}

		out = append(out, qa{Question: question, Answer: answer})
	}

	return out, nil
}
